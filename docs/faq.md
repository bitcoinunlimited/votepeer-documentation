# Frequently Asked Questions (FAQ)

### Do I need the Android app to vote?

Yes

### Can I add a BCH address after the vote has started?

No

### Is it really anonymous or is it pseudonymous?

*"Anonymous" implies the vote is truly private and can't be connected to your identity.*

VotePeer uses traceable ring signature for signing a vote fully anonymously and can be used with CashFusion to make funding the transaction practically untraceable

<hr>

### Is it possible to restrict who can vote while maintaining anonymity?

A list of public keys representing voters is known in advance. Everyone knows this list of voters.

You can't tell who from that list cast which vote. You can tell that a vote was cast by someone on that list.

<hr>

### What is to stop me from voting multiple times in a row? Votes today are based on kyc

Are you asking what is stopping you from technically voting multiple times, or what's stopping you from participating multiple times?

**Multiple votes from one identity**

Technically, everyone can see if you voted multiple times. If you have two or more votes in the same block, you spoil your vote. If you have two or more votes across multiple blocks, the one with the most confirmations is counted.

**Multiple identites**

Participants are added before the vote starts. Every participants needs to provide an Bitcoin Cash address representing their voter identity. It's up to the election "host/master" to decide whom to add or how to filter duplicate participants out. The contract is designed such that the list of all the participants is available to all other participants.

BU/VotePeer Twitter space questions (TBA in FAQ)

### How do you vote with VotePeer?

Most users will use the VotePeer app on their phone. Developers can use the voting protocols in their custom solutions. For developers, we have a JavaScript library and a Kotlin library available.

### How much does it cost to vote?

This depends on the properties of the election such as if it is transparent or anonymous, but usually casting a vote is about USD $0.02 in Bitcoin Cash network fees.

### Can the vote creator fund votes for participants?

A vote creator can fund the participants by sending small amount of Bitcoin Cash to the participants identity addresses. This does not guarantee the participant will use the funding to cast their vote however.

To fund the voter only to cast a vote in their election is technically possible for transparent elections. This is a planned user improvement feature.

### Is VotePeer pseudonymous?

It can be pseudonymous like Bitcoin itself, but it also supports full anonymity. The properties of the elections (pseudonymous or anonymous) is decided by the election creator.

### How is VotePeer pseudonymous?

When using VotePeer in transparent vote, you are using it pseudonymously. You vote with a blockchain identity, an Bitcoin Cash address, but this identity does not need to be tied to your real identity.

### Can you see what other people voted?

Every vote is visible on the public Bitcoin Cash blockchain. With transparent voting, the (VotePeer) identity of the voter is also visible.

### Can you see if your vote has been tallied?

Yes, but how much computer processing this requries depends on the blockchain contract used in the vote. For all elections, it is possible to verify that your vote was tallied by tallying the full election yourself. This is possible because all votes are open on the blockchain.

If the blockchain contract uses Merkle Tally Trees, you can request a proof of your vote being tallied from a tallyer. The tallier shall either provide you with a proof that it was tallied, or proof that it was not tallied. If the tallier refues your request for proof, you cannot trust the result and should tally the election yourself.

### How does votepeer work compared to voting with a piece of paper? && Does VotePeer support in-person voting using voting booths?

It is possible to extend VotePeer to require co-signing by a voting booth. When your vote is co-signed, this proves that you were at a voting booth when the vote was created.

### Can you see if there are fake votes?

Any double voting or voting by non-participants are detected and ignored when tallying. The evidence of attempts at fake votes cannot be removed from the blockchain.

### Does VotePeer protect agains ballot stuffing?

It is impossible for non-participants to cast vote. It is also not possible for participants to cast more than one valid vote. Any invalid votes are ignored.

### Would VotePeer work with a physical voting booth?

A vote requires a digital signature. A physical voting booth would communicate with your device through QR codes or other means to co-sign a digital signature. This proves that you voted at the booths location.

### Is VotePeer the only voting app on a Proof of Work (PoW) chain? && Does VotePeer use CashScript?

The first versions of VotePeer contract used CashScript. The contracts were later simplified and are written directly in Bitcoin Script (the programming language of Bitcoin Cash).

### Can my DAO use VotePeer?

Early research suggests that using VotePeer to create a UTXO based DAO is possible. For example by using Arbitrum verifiers to calculate an off-chain decision and then push that decision onto the blockchain in combination with Merkle Tally Trees for efficient verification.

### Can a representative democracy use VotePeer?

**"Archive votes on VotePeer for each candidate”?**

TBA
