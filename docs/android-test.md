# Android Instrumented testing

> <https://gitlab.com/bitcoinunlimited/votepeer-android-app>

Android instrumented tests require the VotePeer android library to be checked out locally, see [section "Before building"](android-build.md).

## Running from Android Studio

In Run/Debug configurations, click the \[ + ] button and `Android Instrumental Tests`. Select module `VotePeer.app`.

Now you should be able to run the tests by clicking the "Play" button on the toolbar.

### Android Instrumented test results

<img src="https://i.imgur.com/AQVKjkQ.png">

## Running from command line

```bash
./gradlew connectedAndroidTest
```

Android developer documentation section on running tests: <https://developer.android.com/studio/test/command-line#RunTestsGradle>

## Jacoco: Code coverage reporting

##### Coverage reports for the unit tests

```shell
./gradlew jacocoTestDebugUnitTestReport

App => {rootDir}/app/build/jacoco/jacoco.xml //unit tests
Library => ${rootDir}../VotePeerLibrary/votepeer/build/jacoco/jacoco.xml //unit tests
```

##### Coverage reports for the UI tests

`See: https://vsahin.com/2021/06/21/coverage-reports-in-android-and-sonarqube/`

```shell
./gradlew createDebugCoverageReport

App => "${rootDir}/app/build/reports/coverage/debug/report.xml", //ui tests
Library => ${rootDir}../VotePeerLibrary/votepeer/build/reports/coverage/debug/report.xml //ui tests
```
