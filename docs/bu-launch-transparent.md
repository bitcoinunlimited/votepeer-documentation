# Bitcoin Unlimited vote January 2022

*Preparation, voting and results for Bitcoin Unlimited vote 21-26 January 2022.*

Bitcoin Unlimited used VotePeer for on-chain voting for BUIP (Bitcoin Unlimited Improvement Proposal) BUIP178 to BUIP181.

The funding-related BUIPs in January 2022 proposed allocating a total of **$226,000** in funding for various R\&D projects. Three different project proposals were budgeting $100,000, $100,000 and $26,000. There was also a BUIP for the presidency of Bitcoin Unlimited.

## Preperation

The BUIPs were proposed by a Bitcoin Unlimited member and then announced on the [Bitcoin Unlimited forum](https://forum.bitcoinunlimited.info/t/announcing-the-next-bu-vote-21-26-january-2022/202).
Before any vote could start, every member had to download the VotePeer Android app from [google play](https://play.google.com/store/apps/details?id=info.bitcoinunlimited.voting) or through [direct download](https://voter.cash). Then Bitcoin Unlimited members use their membership address from bitcoin unlimited to sign a message that contains their VotePeer identity and thus [verify it](https://docs.voter.cash/bu-vote-verifying-votepeer-identity/). Each member also had to fund their VotePeer identity with ~$0.1 worth of BCH to cover transaction costs for voting on-chain.

The president of Bitcoin Unlimited created votes using the [VotePeer website](https://voter.cash). To authenticate to the VotePeer website the VotePeer Android app is required. Upon navigating to the login page on the VotePeer website, the president is presented with a QR-code. From the login-page found in the main menu of the android app. The president scans the QR code. The VotePeer app uses the VotePeer identity to sign a message embedded in the QR-code. The signature is posted to the VotePeer auth service to authenticate the browser and the browser is redirected to the VotePeer dashboard after being athentiated.

The president of Bitcoin Unlimited created votes using the [VotePeer website](https://voter.cash/login) and the required [VotePeer android app](https://voter.cash) to authenticate. Upon navigating to the login page on the VotePeer website, it presents a QR-code. From the login page found in the main menu of the android app. The president scans the QR code found under "login" in the VotePeer Android app. Upon scanning the QR code from the VotePeer website using the VotePeer app. The app VotePeer identity to signs a message embedded in the QR-code. The signature is posted to the VotePeer auth service to authenticate the browser. Upon authentication, the browser is redirected to the VotePeer dashboard.

To finalize preparations, as an election creator, the president of Bitcoin Unlimited has to gather all the VotePeer identities of the participants. The ballots and descriptions for the votes also had to be collected. The description, ballots, and participating identities are entered into the VotePeer website for each vote. Then the vote data is shared to the participant's VotePeer apps as a data blop.

Because none one can add VotePeer identities to a vote after being created, the Bitcoin Unlimited President started the votes only after a  satisfactory amount of VotePeer identities were available in the [Confirmed List of BU Member IDs on VotePeer](https://forum.bitcoinunlimited.info/t/confirmed-list-of-bu-member-ids-on-votepeer/219).

## Vote

As the election creator, the Bitcoin Unlimited presidents use the data blobs gathered for votes to launch them through the VotePeer website.

The VotePeer Android app for each participating identity receives a push notification for each vote created. The notification informs that there is a new BUIP available to vote in. After clicking the push notification and navigating to the vote, the Bitcoin Unlimited member can choose to accept, reject, or vote blank. When the members pressed the "Submit Vote"-button, the VotePeer android app automatically created a voting transaction containing their ballot. The participant could use the VotePeer android app to verify the tally directly from the bitcoin cash blockchain and open the voting transaction in a block explorer. In addition to that, the VotePeer Android displayed a link to a website with the result for each vote.

![](buip_178_181_list_app.png)
![](has_voted.png)
![](tally.png)

[Announcement: VOTING is CLOSED for BUIPs 178 to 181](https://forum.bitcoinunlimited.info/t/voting-is-closed-for-buips-178-to-181/220)

## Result

*   [BUIP178: NEXA Official Website](https://voter.cash/#/election/RQYxl3keRSjgnTWbBAnj)
*   [BUIP179: Ecosystem Outreach & Marketing for BU & On-chain Scaling 2022](https://voter.cash/#/election/qclI7FNgjulm1HO4oKCs)
*   [BUIP180: Andrew Clifford for President](https://voter.cash/#/election/0APBsoMoTfbufqps9lIL)
*   [BUIP181: BU Mobile Wallet Project](https://voter.cash/#/election/JDlT4ehYUG1AsrxcnDsB)

## Lessons learned

*   The [Confirmed List of BU Member IDs on VotePeer](https://forum.bitcoinunlimited.info/t/confirmed-list-of-bu-member-ids-on-votepeer/219) could have started sooner. Start gathering VotePeer identities in good time—for example, a month before each election start.
*   Discovered UI-bug: Add support for titles that do not collide with the eye icon
*   Discovered a Critical bug: Enables users to vote twice and spoiled their vote unintentionally.
