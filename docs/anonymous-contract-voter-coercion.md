# Voter Coercion with Anonymous Voting

    Version: 1.0 DRAFT
    Authors: dagurval,
    Date: 2022-03-02

This article discusses the challenges with voter coercion with anonymous
voting contracts such as the
[traceable ring signature contract](ring-signature-vote-contract.md).

## Background

The first *Voting System Requirement* in Hjálmarsson's
[Blockchain-Based E-Voting System](#ref-1) paper is as follows:

> (i) An election system should not enable coerced voting.

Voter coercion includes intimidating, threatening or otherwise coercing
another person to not vote for the candidate of their choice.

## Coercion vector: Remote Voting

Remote elections provide no coercion resistance because of the non supervised
factor in a remote election \[[1](#ref-1)].

This coercion vector can be removed by requiring physical voting booths, like
with traditional voting. To enforce the requirement we can extend the smart contract to require the hash of the vote to be co-signed in the voting booth, by the voting booth. This forces the user to cast his vote at a supervised location.

In this scenario, the voting booth has a device that communicates with the users voting device (if using VotePeer, a mobile phone\[[2](#ref-2)]). The data needed to communicate is limited to few bytes (32 bytes for a hash, 64 bytes for a signature) and can be something simple like QR code.

## Coercion vector: Cryptographic proof of how you voted

An anonymous voting system needs to be able to track attempts at double votes. If a user attempts to vote a second time he will exposed. This exposure happens by tracing the original vote to him. It is provably signed by the same private key.

This ability to expose an attempt at double vote also means that a user is able to proof how he voted. **By signing a second vote he exposes his original vote**.

Conceptually, this could be mitigated by having voting booths (See section "Coercion vector: Remote Voting") where the booth has to co-sign the vote itself. The voting booth would refuse to co-sign a double vote. With this constraint, a voter is unable to cryptographically proof which vote was his. He is able to lie to a coercer what he voted on. A downside is that this adds a new attack vector: The booth can see what you are voting for and if rigged, can refuse to co-sign the vote.

**Research question: Can threshold signature, such as MuSig\[[3](#ref-3)], be applied to ring signatures, where a vote must be signed by 2-of-2 parties? (Party 1 is the voter, party 2 is the voting booth)**

## Coercion vector: Correlating time of vote with network submission

If a coercer has knowledge on when his victim will cast a vote, he can observe the blockchain to see the timing of submitted votes and correlate that with the timing of his victim.

This can be mitigated by submitting the vote to the network at a random interval later after casting the vote. A voter can either submit the vote transaction themselves, or pass it to a truster third party that takes the responsiblity to submit the vote to the network at some interval later.

## References

#### \[1] <a name="ref-1"></a>[Blockchain-Based E-Voting System](https://skemman.is/bitstream/1946/31161/1/Research-Paper-BBEVS.pdf), Friðrik Þ. Hjálmarsson

#### \[2] <a name="ref-2"></a>[VotePeer - Censorship resistat blockchain voting](https://voter.cash)

#### \[3] <a name="ref-3"></a>[Simple Schnorr Multi-Signatures with Applications to Bitcoin](https://eprint.iacr.org/2018/068.pdf)
